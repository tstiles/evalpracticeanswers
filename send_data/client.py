import socket
import re
from student_helper import *

# Refer to README.md for the problem instructions

portpair = ('127.0.0.1', 5000) # webserver is running on port 5000 of the grader


def send_the_data():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect(portpair)
        s.sendall(serialize_dict_to_data(SCORES))
        data = s.recv(1024)
        data_string = data.decode("utf-8")
        data_message = data_string.split('"')[1]
        data_num = [int(s) for s in re.findall(r'-?\d+\.?\d*', data_string)]
        return (data_message, data_num)
if __name__ == '__main__':
    send_the_data()