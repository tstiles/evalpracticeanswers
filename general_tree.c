#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "TestCode.h"

// Refer to README.md for the problem instructions

// int nums[] = {0,4,4,2,4,5,4,6,0,12,0,14,14,77,14,56};

struct TreeNode * getLastSibling(struct TreeNode *cursor)
{
    while(cursor->nextSibling != NULL)
    {
        cursor = cursor->nextSibling;
    }
    return cursor;
}

void appendChild(struct TreeNode *cursor, struct TreeNode *node)
{
    while(cursor != NULL)
    {
        // find the level 1 node that matches with current node
        if(cursor->number == node->parent_number)
        {
            // if the level 1 node has no child we give it one
            if(cursor->firstChild == NULL)
            {
                cursor->firstChild = node;
                return;
            }
            // if it has a child we go through its siblings and append it to the end
            cursor = cursor->firstChild;
            cursor = getLastSibling(cursor);
            cursor->nextSibling = node;
            return;
        }
        cursor = cursor->nextSibling;
    }
}

struct TreeNode *buildTree(int nums[], int size)
{
    // initialize root
    struct TreeNode *root = (struct TreeNode *)calloc(1, sizeof(struct TreeNode));
    struct TreeNode *cursor = root;
    
    // assigns roots first numbers
    root->parent_number = nums[0];
    root->number = nums[1];

    for(int i = 2; i<size - 2; i += 2) // start @ 2 because we made root, step by 2 for parent and num
    {
        struct TreeNode *new_node = (struct TreeNode *)calloc(1, sizeof(struct TreeNode));
        new_node->parent_number = nums[i];
        new_node->number = nums[i + 1];

        // check if level 1 node
        if(new_node->parent_number == root->parent_number)
        {
            // appending as next sibling on level 1
            cursor = getLastSibling(cursor);
            cursor->nextSibling = new_node;
        }
        else // its now a level 2 node and will be put into proper position
        {
            appendChild(cursor, new_node);
        }
        cursor = root; // reset cursor position
    }

    return root;
}
