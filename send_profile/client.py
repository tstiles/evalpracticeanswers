import json
import pickle
import socket

# Refer to README.md for the problem instructions

#host = "network-server"
host = "127.0.0.1"
port = 5000


def read_user_info(file_name):

    try:
        with open(file_name, "r") as f:
            valid = True
            data = f.read()
            parsed_data = json.loads(data)

            if 'usr_cred' not in parsed_data:
                valid = False
            if 'u_name' not in parsed_data:
                valid = False
            if 'total_posts' not in parsed_data:
                valid = False
            
            if not valid:
                return "Invalid format"

            return parsed_data
    except FileNotFoundError:
        return "File does not exist"
    except json.JSONDecodeError:
        return  "Invalid format"

def send_user_data(data) -> str:
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        json_data = data["usr_cred"].encode()
        s.connect((host,port))
        s.sendall(json_data)
        resp = s.recv(1024).decode("utf-8")
        if(resp == "Authentication successful"):
            user_data = {}
            for i, v in data.items():
                if i == "usr_cred":
                    continue
                user_data[i] = v
            pickle_data = pickle.dumps(user_data)
            s.sendall(pickle_data)
            resp = s.recv(1024).decode("utf-8")
            return resp
        else:
            print(resp)
            return resp


def logon(file_name):
    user_info = read_user_info(file_name)
    if not isinstance(user_info, dict):
        return user_info
    return send_user_data(user_info)