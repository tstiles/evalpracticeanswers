#include <pthread.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "TestCode.h"

#define MAX_THREADS 10
#define PRIME_CAPACITY 10000

// Refer to README.md for the problem instructions

typedef struct t_data 
{
    int *input;
    int *output;
} t_data;

void * isPrime(void *numbers)
{
    t_data *n_data = (t_data *)numbers;
    int *n = n_data->input;
    int maxWork = 1000;
    int *primes = calloc(maxWork, sizeof(int));
    n_data->output = primes;
    int flag = 0;
    int nPos = 0;
    for(int i = 0; i < maxWork; i++)
    {
        flag = 0;
        if(n[i] == 0 || n[i] == 1)
            continue;
        for(int j = 2; j <= n[i]/2 ; j++)
        {
            if(n[i]%j == 0)
            {
                flag = 1;
                break;
            }
        }

        if (flag == 1)
            continue;
        
        primes[nPos] = n[i];
        nPos += 1;
    }
    pthread_exit(NULL);
}

int FindPrimes(const char *fname, int *primes, int numSize)
{
    pthread_t threads[MAX_THREADS];
    int numbers [numSize];
    
    if (fname == NULL || primes == NULL || numSize <= 0)
    {
        return 0;
    }


    FILE *fp = fopen(fname, "r");
    if(!fp)
        return 0;
    for (int i = 0; i<numSize; i++)
    {
        fscanf(fp, "%i", &numbers[i]);
    }    

    int workSize = numSize/MAX_THREADS + numSize%MAX_THREADS;
    int workQ [MAX_THREADS][workSize];
    
    for
    (
        int primeIterator, threadIterator, cellIterator = 0; 
        primeIterator < numSize; 
        primeIterator++
    )
    {
        if(threadIterator > 9)
        {
            threadIterator = 0;
            cellIterator += 1;
        }
            
        workQ[threadIterator][cellIterator] = numbers[primeIterator];

        threadIterator += 1;
    }

    t_data *work_data = calloc(MAX_THREADS, sizeof(t_data));

    for (int i = 0; i < MAX_THREADS; i++)
    {
        work_data[i].input = workQ[i];
        pthread_create(&threads[i], NULL, isPrime, &work_data[i]);
    }

    for (int i = 0; i < MAX_THREADS; i++)
    {
        pthread_join(threads[i], NULL);
    }

    int index = 0;
    for(int i = 0; i < MAX_THREADS; i++)
    {
        for(int j = 0; j < workSize; j++)
        {
            if(work_data[i].output[j] != 0)
            {
                primes[index] = work_data[i].output[j];
                index += 1;
            }
        }
    }


    return 1;
}
