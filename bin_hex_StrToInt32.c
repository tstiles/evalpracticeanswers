#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include "TestCode.h"

// Refer to README.md for the problem instructions

int power(int base, int exponent)
{
    int total = 1;

    if (exponent == 0)
        return total;
    
    for ( int i = 0; i < exponent; i++)
    {
        total *= base;
    }

    return total;
}

int hexCharToInt(char c)
{
    char *hex_key = "0123456789ABCDEF";

    for ( int i = 0; i < 16; i++ )
    {
        if(c == hex_key[i])
            return i;
    }
    return -1;
}

int bin_hex_StrToInt32(const char *s, int mode)
{
    char *hex_key = "0123456789ABCDEF";
    char *s_upper;
    // check if valid mode
    if ( mode == 1 || mode == 2)
    {
        // do nothing
    }
    else
    {
        return ERROR_INVALID_PARAMETER;
    }

    // check if null
    if ( s == NULL )
    {
        return ERROR_INVALID_PARAMETER;
    }

    int s_size = strlen(s);

    if ( s_size <= 0 )
    {
        return ERROR_INVALID_PARAMETER;
    }

    // make s str become upper case
    s_upper = calloc(s_size, sizeof(char));
    for ( int i = 0; i < s_size; i++ )
    {
        s_upper[i] = toupper((unsigned char) s[i]);
    }

    // check for invalid data in binary strings
    if ( mode == 1 )
    {
        for (int i = 0; i < s_size; i++)
        {
            char c = s[i];

            if (c == '0' || c == '1')
            {
                continue;
            }
            else
            {
                return ERROR_INVALID_PARAMETER;
            }
        }
    }

    // check for invalid data in hex strings
    if ( mode == 2 )
    {
        for (int i = 0; i < s_size; i++)
        {
            char c = s_upper[i];

            for (int j = 0; j < 16; j++)
            {
                if(j == 15)
                {
                    if( hex_key[j] != c )
                        return ERROR_INVALID_PARAMETER;
                }

                if( hex_key[j] == c )
                {
                    break;
                }
            }
        }
    }

    // calculate a bin string
    if( mode == 1 )
    {
        long total = 0;

        for ( int i = s_size - 1, j = 0; i >= 0; i--, j++ )
        {
            int binDigit = s[i] - '0';
            total += binDigit * power(2, j);
        }

        return total;
    }

    // calculate hex string
    if ( mode == 2 )
    {
        int total = 0;

        for( int i = s_size - 1, j = 0; i >= 0; i--, j++ )
        {
            int hexInt = hexCharToInt(s_upper[i]);
            total += hexInt * power(16, j);
        }

        return total;
    }
    return 0;
}
