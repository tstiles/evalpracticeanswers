#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "testcases.h"
#include "TestCode.h"

// Refer to README.md for the problem instructions

int fileDump(const char *fname, const char *words[], int wordLen)
{
    if(fname == NULL || words == NULL)
        return 0;

    if(wordLen <= 0)
    {
        FILE *fp = fopen(fname, "w");
        if(fp == NULL)
            return 0;
        fclose(fp);
        return 1;    
    }    

    FILE *fp = fopen(fname, "w");

    if(fp == NULL)
        return 0;

    for (int i = 0; i < wordLen; i++)
    {
        if(EOF == fputs(words[i], fp))
            return 0;
        
        if(EOF == fputc('\n', fp))
            return 0;
    }
    fclose(fp);
    return 1;
}

int main(int argc, char **argv)
{

    // check for command line parameter and track its position
    char filenameFlag = 0;
    char filenameFlagPos = 0;
    char *fileNameValue = "text.txt";
    for (int i = 1; i < argc; i++)
    {
        if(0 == strncmp(argv[i], "-f", strlen("-f"))
            || 0 == strncmp(argv[i], "--filename", strlen("--filename")))
        {
            filenameFlag = 1;
            filenameFlagPos = i;
            if(i+1<argc)
            {
                fileNameValue = argv[i+1];
                break;
            }            
        }
    }

    char **words;
    words = calloc(argc, sizeof(char *));

    int wordLen = 0;
    for(int i = 1; i < argc; i++)
    {
        if(filenameFlag)
        {
            if(i == filenameFlagPos || i == filenameFlagPos + 1)
                continue;
        }
        words[wordLen]= argv[i];
        wordLen += 1;
    }

    if( 1 == fileDump(fileNameValue, words, wordLen))
    {
        free(words);
        return 0;
    }

    return 1;  //You may need to change this line
}
