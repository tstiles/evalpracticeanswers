# Refer to README.md for the problem instructions


def convertAndShift(bStr):
    if len(bStr) > 16 or len(bStr) < 16:
        return 'INVALID_LENGTH'

    for c in bStr:
        if c == '0' or c == '1':
            continue
        else:
            return 'INVALID_VALUE'

    exp = [2**exp for exp in range(len(bStr)-1, -1, -1)]
    bInt = sum([int(bStr[i]) * exp[i] for i in range(len(bStr))])

    if bInt % 2 == 0:
        bInt = bInt << 2
    else:
        bInt = bInt >> 1

    if bInt > 200:
       bInt = ~bInt
    return bInt
