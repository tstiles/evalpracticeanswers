# Refer to README.md for the problem instructions

def ensureBinFormat(bin)->bool:
    bin = str(bin)
    for i in bin:
        if i == '0' or i == '1':
            continue
        else:
            return False
    return True

def binary_to_decimal(binaryNumber):
    if not ensureBinFormat(binaryNumber):
        return "Invalid Input"

    strbin = str(binaryNumber)
    
    exp = [ 2**exp for exp in range(len(strbin) -1, -1, -1)]
    exp = [int(strbin[digit]) * exp[digit] for digit in range(len(strbin))]
    return sum(exp)
    #return sum([int(digit) * exp for digit in strbin])
