#include <stdio.h>
#include <math.h>

// Refer to README.md for the problem instructions

int buyGroceries(int stuff[], int size)
{
    // check valid initialization
    if ( stuff == NULL || size <= 0 || size % 2 != 0)
        return 0;

    for (int i = 0; i < size; i+=2)
    {
        // check if items valid
        if(stuff[i] < 1 || stuff[i] > 4)
            return 0;

        // check if quantity is valid
        if(stuff[i+1] <= 0)
            return 0;
    }

    float total = 0;    
    for (int i = 0; i < size; i+=2)
    {
        float item_cost = 0;
        switch(stuff[i])
        {
            case 1:
                item_cost = 3.50;
                break;

            case 2:
                item_cost = 2.25;
                break;

            case 3:
                item_cost = 1.99;
                break;
            
            case 4:
                item_cost = 4.15;
                break;
        }

        int quantity = stuff[i+1];
        if ( quantity >= 5)
        {
            float discount = (item_cost * quantity ) * 0.05;
            total += (item_cost * quantity ) - discount;
            continue;
        }
        else
        {
        total += item_cost * quantity;
        }
    }

    return round(total);
}
