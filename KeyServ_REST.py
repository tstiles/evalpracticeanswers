#!/usr/bin/env python

import socket, http.client, re

http_string = "GET / HTTP/1.1\r\n\r\n"
portpair = ('network-server', 5000) # webserver is running on port 5000 of the grader

g_user_id = 'User1'

# Refer to README.md for the problem instructions


def do_key():
    client = http.client.HTTPConnection('127.0.0.1', 5000)
    client.request('KEY', f'/user/{g_user_id}/')
    res = client.getresponse()

    if res.status != 200:
        return None
    
    key = res.read()

    return key


def do_put(key_val) -> str:
    client = http.client.HTTPConnection('127.0.0.1', 5000)
    key_val = key_val.decode('utf-8')
    # ex: PUT /key/\<user_id\>/\<key_val\>/ HTTP/1.1 
    client.request('PUT', f'/key/{g_user_id}/{key_val}/')
    res = client.getresponse()
    server_message = res.read().decode('utf-8')

    print(server_message)

    return server_message

if __name__ == '__main__':
    client = http.client.HTTPConnection('127.0.0.1', 5000)
    client.request('KEY', f'/userid/{g_user_id}/', headers={})
    res = client.getresponse()

    print(res)
