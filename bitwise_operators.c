#include <stdio.h>
#include <string.h>
#include <math.h>
#include "TestCode.h"

// Refer to README.md for the problem instructions

int checkBinOnly(const char *s)
{
    for (int i = 0; i < strlen(s); i++)
    {
        char c = s[i];
        if( c == '0' || c == '1')
        {
            continue;
        }
        else
        {
            return -1;
        }
    }
    return 0;
}

int checkValid(const char *x, const char *y)
{
    if( x == NULL || y == NULL)
    {
        return -1;
    }

    int x_strlen = strlen(x);
    int y_strlen = strlen(y);

    if(x_strlen < 16 || x_strlen > 16)
        return -1;

    if(y_strlen < 16 || y_strlen > 16)
        return -1;

    if(checkBinOnly(x) != 0)
        return -2;

    if(checkBinOnly(y) != 0)
        return -2;

    return 0;
}

int binConvert(const char *bin)
{
    int binValue = 0;
    int power = 0;
    for (int i = 15; i >= 0; i--, power++)
    {
        binValue += (bin[i] - '0') * pow(2, power);
    }
    return binValue;
}

int bitwiseOps(const char *first, const char *second)
{
    int status = checkValid(first, second);
    if (status != 0)
        return status;

    int fVal = binConvert(first);
    int sVal = binConvert(second);

    if(fVal % 2 == 0 && sVal % 2 == 0)
        return fVal & sVal;
    
    if(fVal % 2 != 0 && sVal % 2 != 0)
        return fVal | sVal;

    if(fVal > 255 && sVal > 255)
        return fVal ^ sVal;

    return fVal + sVal;
}
