import socket

#host = "network-server"
host = "localhost"
port = 5000
srv_port = 1337


def contact_clients():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((host, port))
        sent = s.send(str(srv_port).encode("UTF-8"))
        if sent == 0:
            raise RuntimeError("Could not connect")
        s.close()
    

def serve_clients():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:        
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind(("localhost", srv_port))
        s.listen(5)
        contact_clients()
        for i in range(2):
            clientSock, addr = s.accept()
            recv = clientSock.recv(1024)
            textFile = recv.decode("UTF-8")
            filename = textFile.split("\n")[0]
            with open(filename, "w") as f:
                data = textFile.split("\n")[1:-1]
                dataToWrite = ""
                for line in data:
                    dataToWrite += line + "\n"
                f.write(dataToWrite)
            clientSock.close()

    s.close()