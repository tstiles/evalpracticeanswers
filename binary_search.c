#include <stdio.h>
#include "TestCode.h"

// Refer to README.md for the problem instructions


int BSearch(int arr[], int left, int right, int target)
{
    while ( left <= right )
    {
        int middle = (int)((left + right) / 2);
        int cursor = arr[middle];

        if (target == cursor)
            return middle;

        // scan right
        if (cursor < target)
        {
            left = middle + 1;
        }
        else
        // scan left
        {
            right = middle - 1;
        }
    }

    if (arr[left] == target)
        return left;
    return -1;
}
