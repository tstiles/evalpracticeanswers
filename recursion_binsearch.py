# Refer to README.md for the problem instructions


def createOrderedList(uList):
    oList = []
    while(len(uList) != 0):
        indexMin = uList.index(min(uList))
        oList.append(uList[indexMin])
        uList.pop(indexMin)
    return oList
                


def binarySearch (lst, start, end, val):
    
    if(len(lst) <= 0):
        return -2

    if(start > end):
        return -1

    if(lst[start] == val):
        return start

    return binarySearch(lst, start+1, end, val)


if __name__ == '__main__':
    # [0, 1, 2, 3, 8, 9, 11, 22, 44, 90]
    x = createOrderedList([8, 2, 3, 9, 1, 44, 22, 11, 90, 0])
    print(f'createOrderedList : {x}')

    x = binarySearch(x, 0, len(x), 44)
    print(f'binarySearch : {x}')