/* See README.md for instructions*/
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "TestCode.h"

// Refer to README.md for the problem instructions

nameNode *buildList(const char **names, int size)
{
    int status = 0;
    // NULL CHECKS
    if(names == NULL || size <= 0)
    {
        return NULL;
    }

    // EMPTY STR CHECK
    for (int i = 0; i < size; i++)
    {
        if(names[i])
        {
            if(strlen(names[i]) > 0)
                status = 1;
        }
    }

    if (status != 1)
        return NULL;

    nameNode* head = NULL;
    nameNode* next = NULL;

    for (int i = 0; i < size; i++)
    {
        // Check STR before allocation
        if(names[i] == NULL)
            continue;
        if(strlen(names[i]) <= 0)
            continue;
        
        // Make new head
        head = (nameNode *) calloc(1, sizeof(nameNode));
        head->name = (char *) calloc(strlen(names[i]), sizeof(char));
        strncpy(head->name, names[i], strlen(names[i]));

        // Set head's next node
        head->next = next;
        if(next) // as long as next is not null
            next->prev = head;
        
        // if theres another node we will save current node 
        next = head;
    }
    return head;
}

void freeMemory(nameNode *head)
{
    while(head != NULL)
    {
        head = head->next;
        if(head->next == NULL)
        {
            free(head);
            return;
        }
        free(head->prev);
    }
}

nameNode *removeNode(nameNode *head, const char *findName)
{
    nameNode *deleteNode;
    while(head != NULL)
    {
        int match = strncmp(findName, head->name, strlen(head->name));
        if(match == 0)
        {
            free(head->name);
            deleteNode = head;
            // link front and back nodes
            if(head->prev != NULL && head->next != NULL)
            {
                head->prev->next = head->next;
                head->next->prev = head->prev;
                while(head->prev != NULL)
                    head = head->prev;
                break;
            }

            // handle if head
            if(head->prev == NULL && head->next != NULL)
            {

                head->next->prev = NULL;
                head = head->next;
                break;
            }

            // handle tail
            if(head->prev != NULL && head->next == NULL)
            {
                head->prev->next = NULL;
                while(head->prev != NULL)
                    head = head->prev;
                break;
            }
            

            // handle 1 item list
            if(head->prev == NULL && head->prev == NULL)
            {
                free(head);
                return NULL;
            }
            free(deleteNode);
        }
        if(head->next == NULL)
        {
            while(head->prev != NULL)
                head = head->prev;
            break;            
        }
        head = head->next;
    }
    return head;
}