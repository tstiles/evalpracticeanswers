#include <stdio.h>

// Refer to README.md for the problem instructions

/*
The function bubbleSort receives a pointer to a function, which performs either ascending or descending sorting; an 
integer array; and the size of the array as input arguments. The parameter `compare` is a pointer to the comparison 
function that determines the sorting order. The function returns an integer pointer to the sorted array.
*/

int *bubbleSort(int elements[], size_t length, int(*compare)(int a, int b))
{
    int sorted = 0;
    int storage = 0;
    do
    {
        
        sorted = 0;
        for (int i = 0; i < length - 1; i++)
        {
            if( compare(elements[i], elements[i + 1]) == 1)
            {
                sorted = 1;
                storage = elements[i];
                elements[i] = elements[i + 1];
                elements[i + 1] = storage;
            }
        }
    } while(sorted);

    return elements;
}


// @brief Determine whether elements are out of order for an ascending order sort
// @return Boolean indicating whether the two elements should be swapped
int ascending(int a, int b)
{
    if ( a > b)
        return 1;
    return 0;         
}

// @brief Determine whether elements are out of order for a descending order sort
// @return Boolean indicating whether the two elements should be swapped
int descending(int a, int b)
{
    if (b > a)
        return 1;
    return 0;      
}