# Refer to README.md for the problem instructions


def computeChange(change):
    for val in change.values():
        if val < 0:
            return 0

    fries = 1.50
    soda  = 1.00
    both  = 2.50
    
    '''
    h = 0.50 * change['H']
    q = 0.25 * change['Q']
    d = 0.1  * change['D']
    n = 0.05 * change['N']
    p = 0.01 * change['P']
    '''
    total = 0

    for key in change:
        if(key == 'H'):
            total += change['H'] * 0.50
        if(key == 'Q'):
            total += change['Q'] * 0.25
        if(key == 'D'):
            total += change['D'] * 0.1
        if(key == 'N'):
            total += change['N'] * 0.05
        if(key == 'P'):
            total += change['P'] * 0.01
    
    total = round(total, 2)

    if total >= both:
        return (total, 'BOTH')
    
    if total >= fries:
        return (total, 'FRIES')

    if total >= soda:
        return (total, 'SODA')

    return (total, 'NOTHING')
