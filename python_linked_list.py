from typing import Iterable
from myNode import Node
# Refer to README.md for the problem instructions


class SingleList():
    def __init__(self, name_list: Iterable = None) -> None:
        self.name_list = name_list
        if(name_list == None):
            self.head = None
            self.tail = None
            return
        self.head = Node(name_list[0])
        
        cursor = self.head
        for name in name_list[1:]:
            cursor.next = Node(name)
            cursor = cursor.next
        self.tail = cursor

    def remove(self, name: str) -> str or None:
        cursor = self.head
        if name == None:
            while(cursor.next.next != None):
                cursor = cursor.next
            self.tail = cursor
            name = self.tail.next.name
            self.tail.next = None
            return name

        prev_node = None
        while(cursor != None):
            if(cursor.next == None and cursor.name != name):
                return None

            # try statement incase were checking null
            if(cursor.next != None):
                if(cursor.next.name == name):
                    prev_node = cursor

            if(cursor.name == name and prev_node != None):
                prev_node.next = cursor.next
                return name

            if(cursor.name == name and prev_node == None):
                self.head = self.head.next
                return name
            
            cursor = cursor.next
            

        


    def insert(self, name: str, position: int = None):
        node = Node(name)
        cursor = self.head

        if self.head == None:
            self.head = node
            self.tail = node
            return

        # insert at beginning
        if(position == 0):    
            node.next = self.head
            self.head = node
            return

        # insert at end
        if(position == None):
            # iterate to last node
            while(cursor.next != None):
                cursor = cursor.next
            cursor.next = node
            self.tail = node
            return

        # insert at position
        prev_node = None
        for pos in range(position):
            if (pos + 1 == position):
                prev_node = cursor
            cursor = cursor.next
        prev_node.next = node
        node.next = cursor
            



