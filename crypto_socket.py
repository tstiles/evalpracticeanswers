import socket
import re
import binascii
from cryptography.fernet import Fernet

# Refer to README.md for the problem instructions

HOST = 'network-server'  # The server's hostname or IP address
HOST = '127.0.0.1'
PORT = 5000        # The port used by the server


# Your create_fernet_and_find_sign_key function here
def create_fernet_and_find_sign_key(master_key):
    try:
        fernet_key = Fernet(master_key)
        signing_key = fernet_key._signing_key

        return (fernet_key, signing_key)
    except ValueError as e:
        raise ValueError('Potential Intrusion Event Detected.') from e

def dowork():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        # connecto server recv key
        s.connect((HOST, PORT))
        data = s.recv(1024)

        try:
            master_fernet, signing_key = create_fernet_and_find_sign_key(data)
        except ValueError as e:
            return e.args[0]
        
        s.sendall(signing_key)

        # parse server response too see if key valid
        data = s.recv(1024)        

        if data.decode("utf-8") == "Wrong signing key":
            s.close()
            return data.decode("utf-8")
        else:
            s.sendall(master_fernet.decrypt(data))

        # print decrypted message
        decrypted_message = master_fernet.decrypt(data)
        print(decrypted_message.decode('utf-8'))

        # listen for server to say success or fail
        data = s.recv(1024)

        return decrypted_message

def main():
    dowork()


if __name__ == '__main__':
    main()
