#include "TestCode.h"
#include "stack.h"
#include <stdio.h>
#include <stdlib.h>

/** 
 * Refer to README.md for the problem instructions
 * 
 * @param tree_ptr a pointer to the tree that you will calculate the sum of all nodes for
 * @return the sum of all nodes in the tree. If there is an error, return TREE_ERROR instead
 */

void get_stack(p_node root, p_stack stack)
{
    if(root != NULL)
        get_stack(root->left, stack);
    if(root != NULL)
        get_stack(root->right, stack);
    
    push(stack, (void *) root);    
}

int sum_stack(p_stack stack)
{
    int sum = 0;
    p_stack_elem cursor = stack->top;
    while(cursor != NULL)
    {
        //sum += cursor->stored_item->key;
        node *tmp_node = (node *)cursor->stored_item;
        sum += tmp_node->key;
        cursor = cursor->next;
    }
    return sum;
}

int calculate_tree_sum(const p_tree tree_ptr) {
    if(tree_ptr == NULL)
        return TREE_ERROR;

    if(tree_ptr->root == NULL)
        return TREE_ERROR;

    p_stack stack = (p_stack)calloc(1, sizeof(stack));
    get_stack(tree_ptr->root, stack);
    return sum_stack(stack);
}
