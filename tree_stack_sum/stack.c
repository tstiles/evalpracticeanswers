#include "stack.h"

#include <stdlib.h>
#include <string.h>


/* 
 * This function will take a value and push it onto the stack
 * @param stack_ptr a pointer to the stack that you will push elements onto.
 * @param stored_value a pointer to the value in a tree's node.
 * @return return STACK_OPERATION_SUCCESS if successful, return STACK_OPERATION_ERROR if error
 */
int push(p_stack stack_ptr, void *stored_value) {
    if(stack_ptr == NULL || stored_value == NULL)
        return STACK_OPERATION_ERROR;

    // create new node load val into it
    p_stack_elem node = (p_stack_elem) calloc(1, sizeof(stack_elem));
    node->stored_item = stored_value; // load saved val
    node->next = stack_ptr->top; // set next to whats going to be the old top

    stack_ptr->top = node; // assigning new top

    // add to the stack size since new element is in
    stack_ptr->size += 1;

    return STACK_OPERATION_SUCCESS;
}

/* 
 * This function will take pop the top element off of the stack and assign the value coming off to item_value_ptr
 * @param stack_ptr a pointer to the stack that you will pop elements from.
 * @param item_value_ptr a pointer to a tree's node.
 * @return return STACK_OPERATION_SUCCESS if successful, return STACK_OPERATION_ERROR if error
 */
int pop(p_stack stack_ptr, void **item_value_ptr) {
    if(stack_ptr == NULL)
        return STACK_OPERATION_ERROR;

    p_stack_elem tmp = stack_ptr->top;

    *item_value_ptr = stack_ptr->top; // assign the value to give back
    // change top of stack
    if(stack_ptr->top->next != NULL)
    {
        stack_ptr->top = stack_ptr->top->next; // change the top value to the object under
    }
    else
    {
        stack_ptr->top = NULL; // assign null if last element was popped
    }
    

    free(tmp);
    return STACK_OPERATION_SUCCESS;
}

/* 
 * This function will allocate a new stack.
 * @return returns a new stack
 */
p_stack stack_create() {
    
    p_stack const new_stack = (p_stack)calloc(1, sizeof(stack));
    return new_stack;
}

/* 
 * This function will pop and free all elements of the stack
 * @return return STACK_OPERATION_SUCCESS if successful, return STACK_OPERATION_ERROR if error
 */
int stack_destroy(p_stack const stack_ptr) {
    if(stack_ptr == NULL)
        return STACK_OPERATION_ERROR;
    
    p_stack_elem head = stack_ptr->top;
    p_stack_elem tmp = NULL;

    while(head->next != NULL)
    {
        tmp = head;
        head = head->next;
        free(tmp);    
    }

    free(head);

    return STACK_OPERATION_SUCCESS;
}
