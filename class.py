import math
import sys
# Refer to README.md for the problem instructions

class Player(object):
    def __init__(self, name, health=100, x_pos=0, y_pos=0) -> None:
        self.name = name
        self.health = health
        self.x_pos = x_pos
        self.y_pos = y_pos

    def report_pos(self):
        report = (self.x_pos, self.y_pos)
        return report

    def reduce_health(self, dmg):
        self.health = self.health - (dmg/2)
        
        if self.health <= 0.0:
            self.health = 0

    def move(self, x_pos, y_pos):
        fall_diff = self.y_pos + y_pos
        
        if fall_diff <= -5:
            distance = math.sqrt((self.x_pos - x_pos + self.x_pos)**2 + (self.y_pos - y_pos + self.y_pos)**2)
            distance = abs(distance)
            self.reduce_health(distance)
            if(self.health <= 0):
                #print('You are out of hit points!', file=sys.stdout)
                print('You are out of health!')

        self.x_pos += x_pos
        self.y_pos += y_pos
        return self.health
        #jokem.move(100,-100)
        #self.assertAlmostEqual(29.289321881345245, jokem.health)


