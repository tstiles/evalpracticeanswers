import re

def find_matches(inputString):
    pattern = re.compile(r'((\d{1,3}\.){3}\d{1,3}):(\d{5})')
    matches = pattern.finditer(inputString)
    parsed_matches = []
    for match in matches:
        ip = match.group(1)
        port = int(match.group(3))
        parsed_matches.append((ip, port))
    return parsed_matches

if __name__ == "__main__":
    with open("test.log", "rt") as fp:
        input = fp.read()
        find_matches(input)