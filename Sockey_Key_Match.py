import socket
import random
from random import randint
from time import sleep

# Refer to README.md for the problem instructions

#serverName = "network-server"
serverName = '127.0.0.1'
serverPort = 5000

session_keys = ["aa072fbbf5f408d81c78033dd560d4f6",
                "bb072fbbf5f408d81c78033dd560f6d4",
                "f5072fbbf5f408d81c78033dd5f6d460",
                "408df5072fbbf5f81c3dd5f6d4607803",
                "dd5f408df5072fbbfc36d46078035f81",
                "c36d408df5072fbbf46078035f81dd5f",
                "35f8c36df5072fbbf4607801dd5fd408",
                "2f07aaf408d81c78033dd560d4f6bbf5",
                "80332ff408d81c7dd560d4f6bbf507aa",
                "560d4f8033281c7dd6bbf507aaff408d",
               ]

def xor_strings(xs, ys):
    return "".join(chr(ord(x) ^ ord(y)) for x, y in zip(xs, ys))

def match_keys():
    for session_key in session_keys:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            sock.connect((serverName, serverPort))
            resp = sock.recv(1024) # conn success
            server_key = sock.recv(1024).decode('utf-8') # recv key

            # Keeping this here for posterity sake
            # XORing the unicode value of the strings
            # instead of the binary value of the Hex value itself ( wasted 2 hours of my life realizing that)
            '''
            xor_key = [chr((ord(x) ^ ord(y))) for x,y in zip(session_key, server_key[2:])]
            '''

            if('ERROR' in server_key):
                continue

            # decrypt string ( XOR )
            xor_key = hex(int(session_key, 16) ^ int(server_key, 16))
            xor_key = xor_key.encode()
            sock.sendall(xor_key)

            # send to server
            resp = sock.recv(1024)
            if("Success" in resp.decode('utf-8')):
                return (resp, session_key)