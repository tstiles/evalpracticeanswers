#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "TestCode.h"

// Refer to README.md for the problem instructions

int sort_strings(const void *a, const void *b)
{
    return strcmp(*(const char **)b, *(const char **)a);
}

struct nameNode *processNames(const char **names)
{
    struct nameNode * head = NULL;    
    int size = 10; // known constant from readme

    // check null
    if(names == NULL)
        return NULL;

    // check if has null elements
    for(int i = 0; i < size; i++)
    {
        if(names[i] == NULL)
        {
            return NULL;
        }
    }

    qsort(names, size, sizeof(*names), sort_strings);

    

    for(int i = 0 ; i < size; i++)
    {
        int stringSize = strlen(names[i]);

        if (stringSize <= 0)
        {
            continue;
        }

        // allocate node add data        
        struct nameNode * tmp = NULL;
        tmp = calloc(1, sizeof(struct nameNode));
        tmp->name = calloc(stringSize, sizeof(char));
        strncpy(tmp->name, names[i], stringSize);

        tmp->next = head;

        head = tmp;
    }

    return head;
}

void freeMemory(struct nameNode *head)
{
    struct nameNode *tmp = head;
    while (head->next != NULL)
    {
        head = head->next;
        free(tmp);
        tmp = head;
    }
}
