# Refer to README.md for the problem instructions

def factorial(n):
    fact = 1
    for i in range(1,n+1):
        fact = fact * i
    return fact


def compute_euler():
    #2.71828
    e = 1 + sum([1/factorial(n) for n in range(1,11)])
    e = round(e,5)
    return e
