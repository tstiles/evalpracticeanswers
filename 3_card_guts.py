# Refer to README.md for the problem instructions


def highestThree(hand):
    if(hand.count(hand[0]) == 3):
        return hand[0]
    return 0

def highestTwo(hand):
    for i in hand:
        if(hand.count(i) == 2):
            return i
    return 0    

def highCard(hand):
    high = 0
    for i in hand:
        if i > high:
            high = i
    return high

def handScore(hand):
    highThree = highestThree(hand)
    if highThree > 0:
        return (3, hand[0])

    highTwo = highestTwo(hand)
    if highTwo > 0:
        return(2, highTwo)

    high = highCard(hand)
    return (1,high)
    

def findGutsWinner(hand1, hand2):
    hand1Length = len(hand1)
    hand2Length = len(hand2)

    if(hand1Length < 3 or hand1Length > 3):
        return []
    if(hand2Length < 3 or hand2Length > 3):
        return []

    hand1Score = handScore(hand1)
    hand2Score = handScore(hand2)

    if(hand1Score[0] == hand2Score[0]):
        if(hand1Score[1] > hand2Score[1]):
            return hand1
        return hand2

    if(hand1Score[0] > hand2Score[0]):
        return hand1
    else:
        return hand2

    

    return []     
