import os

# Refer to README.md for the problem instructions


def writeNewFile(file_name):
    lines = None
    try:
        file_size = os.path.getsize(file_name)
    except FileNotFoundError:
        return -1

    with open(file_name, "r") as f:
        lines = f.readlines()

    if len(lines) <= 0:
        return -2

    with open(f"new{file_name}", "w") as f:
        line_num = 0
        line_max = len(lines)
        for line in lines:
            new_line = ""
            # ensure first char is upper case
            new_line = line.capitalize()

            # handle last line
            if line_num >= line_max - 1:
                # delete newl if at the end of last line
                if line[-1] == "\n":
                    # edge case if it has period before new line
                    if line[-2] == ".":
                        new_line = new_line[:-1]
                        f.write(new_line)
                        os.remove(file_name)
                        return file_size
                    # if no period at the end place one
                    else:
                        new_line += "."
                        f.write(new_line)
                        os.remove(file_name)
                        return file_size
                # checks if it ends with period
                if line[-1] != ".":
                    new_line += "."
                    f.write(new_line)
                    os.remove(file_name)
                    return file_size
                f.write(new_line)
                os.remove(file_name)
                return file_size

            # handle every other line
            
            # checks if already good then skips everything else
            if new_line[-2:] == ".\n":
                f.write(new_line)
                line_num += 1
                continue

            # check if has period but no newl
            if new_line[-1] == ".":
                new_line += "\n"
                f.write(new_line)
                line_num += 1
                continue

            # check if has newl but no period
            if (new_line[-1] == "\n" and
                new_line[-2] != "."):
                new_line = f"{new_line[0:-1]}.\n"
                f.write(new_line)
                line_num += 1
