#include <stdio.h>
#include <string.h>
#include "TestCode.h"

// Refer to README.md for the problem instructions

int validatePassword(const char *password)
{
    // if really wanting to optimize this we would use bit flags instead
    int passwordLength = 0;
    int capitalCount = 0;
    int lowerCaseCount = 0;
    int digitCount = 0;
    int hasSpecialChar = 0;
    int legalCharacter = 0;
    if(password == NULL)
        return 0;
    
    // rule 1 at least and no more than 16 chars longs
    passwordLength = strlen(password);
    if(passwordLength < 8 || passwordLength > 16)
        return 0;

    for (int i = 0; i < passwordLength; i++)
    {
        // rule 2 must contain at least two capital letters
        if(password[i] >= 65 && password[i] <= 90)
        {
            capitalCount += 1;
            legalCharacter = 1;
        }
            


        // rule 3 must contain at least two lower case
        if(password[i] >= 97 && password[i] <= 122)
        {
            lowerCaseCount += 1;
            legalCharacter = 1;
        }
            
        // rule 4 must contain at least two digits
        if(password[i] >= 48 && password[i] <= 57)
        {
            digitCount += 1;
            legalCharacter = 1;
        }
            

        if(password[i] == '!' || password[i] == '#' || password[i] == '$')
        {
            hasSpecialChar = 1;
            legalCharacter = 1;
        }

        if(legalCharacter != 1)
            return 0;

        if (legalCharacter == 1)
            legalCharacter = 0;
    }
    if (capitalCount < 2)
        return 0;
    if (lowerCaseCount < 2)
        return 0;
    if (digitCount < 2)
        return 0;
    if (hasSpecialChar != 1)
        return 0;
    
    return 1;
}
