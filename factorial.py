def factorial (n):
    factorial = 1
    for i in range(1, n+1):
        print(f'{factorial}={factorial}*{i}')
        factorial = factorial * i
    return factorial

def recur_factorial(n):
   if n == 1:
       return n
   else:
       return n*recur_factorial(n-1)

# 120
print(factorial(5))