#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "TestCode.h"

// Refer to README.md for the problem instructions

void insertNode(struct numNode *root, struct numNode* newNode)
{
    if(newNode->val < root->val)
    {
        // go left
        if( root->left != NULL)
        {
            insertNode(root->left, newNode);
        }
        else
        {
            root->left = newNode;
        }
    }

    if(newNode->val > root->val)
    {
        // go right
        if( root->right != NULL)
        {
            insertNode(root->right, newNode);
        }
        else
        {
            root->right = newNode;
        }
    }
}

struct numNode *buildBST(int nums[], int size)
{
    if (size <= 0 || nums == NULL)
        return NULL;

    struct numNode *root = calloc(1, sizeof(struct numNode));
    root->val = nums[0];
    for(int i = 1; i < size; i++)
    {
        struct numNode *newNode = calloc(1, sizeof(struct numNode));
        newNode->val = nums[i];
        insertNode(root , newNode);
    }

    return root;
}




int destroyBST(struct numNode *root)
{
    int count = 1;

    if(root == NULL)
        return 0;

    if(root->left != NULL)
    {
        count += destroyBST(root->left);
    }

    if(root->right != NULL)
    {
        count += destroyBST(root->right);
    }

    free(root);

    return count;
}
