#!/usr/bin/env python

import socket, struct
from cryptography.fernet import Fernet

# Refer to README.md for the problem instructions

#dest = ('network-server', 1337)
dest = ('127.0.0.1', 1337)

cmd_list = {
    'key-request' : 0x800,
    'key-provide' : 0x801,
    'encrypted-message' : 0x802,
    'error' : 0x8FF
}

#
#  You may wish to write helper functions here
# 

#This function should return the server's final message as an actual Python 3 string,
#not as a byte sequence.
def get_message_using_encrypted_request_protocol():
    # do key request
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect(dest)
        key_request = (0x800).to_bytes(4, 'big') + b"\x00" * 44
        s.sendall(key_request)
        data = s.recv(1024)
        key = data[4:]
        f = Fernet(key)
        msg = cmd_list['encrypted-message'].to_bytes(4, 'big')
        msg += f.encrypt(b'I challenge!')
        while(len(msg) < 104):
            msg += b'\x00'
        s.sendall(msg)
        data = s.recv(1024)
        resp = f.decrypt(data)
        return resp.decode('utf-8')

if __name__ == "__main__":
    message = get_message_using_encrypted_request_protocol()
    print(message)
