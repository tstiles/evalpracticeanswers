#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "TestCode.h"

// Refer to README.md for the problem instructions

struct numNode *push(int val, struct numNode *head)
{
    struct numNode *newNode = calloc(1, sizeof(struct numNode));
    newNode->num = val;
    newNode->next = head;
    return newNode;
}

int isDuplicate(struct numNode* head, int val)
{
    while(head != NULL)
    {
        if(head->num == val)
            return 1;
        head = head->next;
    }
    return 0;
}

struct numNode *buildCList(int *nums, int size)
{
    if (nums == NULL || size <= 0)
        return NULL;

    struct numNode *head = calloc(1, sizeof(struct numNode));
    struct numNode *endNode = head;

    head->num = nums[0];
    /*
        int nums4[] = {5, 7,8,7,9,2,34,5,54,1};
        int nums3[] = {5, 7,8,9,2,34,54,1};
    */
    for(int i=1; i < size; i++)
    {
        if(! isDuplicate(head, nums[i]))
            head = push(nums[i], head);
    }

    endNode->next = head;

    return head;
}

int emptyList(struct numNode *head)
{
    int count = 1;
    if (head == NULL) return 0;
    struct numNode* endNode = head;
    struct numNode* tmp;
    while(head->next != endNode)
    {
        printf("%i\n", head->num);
        tmp = head;
        count += 1;
        head = head->next;
        free(tmp);
    }

    free(head);

    return count;
}
