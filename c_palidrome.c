#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

// Refer to README.md for the problem instructions

char sanitize(char c)
{
    // lower case all ascii, ignores spaces and punction
    // RETURN: characters needed only

    // if punctuation or special characters or numbers
    if(
        c <= 64              ||
        (c >= 91 && c <= 96) ||
        c >= 123
    )
        return 0;

    // if capitalized
    if(c >= 65 && c <= 90)
    {
        return c + 32;
    }

    return c;
}

int palindrome(const char *phrase)
{
    if (phrase == NULL)
        return -1;

    if(strlen(phrase) == 0)
        return 0;

    int phraseSize = strlen(phrase);

    char *palindrome = (char *)calloc(phraseSize, sizeof(char));

    // load up palindrome with good chars
    for (int i = 0, j = 0; i < phraseSize; i++)
    {
        char c = sanitize(phrase[i]);
        if (c != 0)
        {
            palindrome[j] = c;
            j++;
        }
    }

    // check if palindrome
    int j = strlen(palindrome);
    for (int i = 0; i < strlen(palindrome); i++)
    {
        if(palindrome[i] != palindrome[j-1])
            return 0;
        j--;
    }
    return 1;
}
