#!/usr/bin/env python
# Refer to README.md for the problem instructions

import socket, struct

#dest = ('network-server', 1337)
dest = ('127.0.0.1', 1337)
accept_port_1 = 12345
accept_port_2 = 54321

cmd_list = {
    'success' : 0x800,
    'error' : 0x801,
    'query_token' : 0x802,
    'get_secret' : 0x803,
}

padding = 0x0000

def handle_response(resp):
    resp_cmd = int(resp[:4].hex(),16)
    resp_payload_length = int(resp[4:5].hex(), 16)
    if(resp_cmd not in cmd_list.values()):
        raise ValueError("The message did not follow the protocol")

    if(resp_payload_length != len(resp[5:])):
        raise ValueError("The message did not follow the protocol")

    resp_payload = None
    if(resp_payload_length == 4):
        resp_payload = int(resp[5:].hex(), 16)
    else:
        resp_payload = resp[5:]
    return (resp_cmd, resp_payload)

def query_token():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        pack_seq = '!LBL'
        msg = struct.pack(pack_seq, cmd_list['query_token'], 4, padding)
        s.bind(('127.0.0.1', accept_port_1))
        host = ('127.0.0.1', 1337)
        s.connect(host)
        s.sendall(msg)
        resp = s.recv(1024)
        code, size, payload = struct.unpack(pack_seq, resp)
        return(code, payload)


def get_secret(token):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        #pack_seq = '!LBL'
        #msg = struct.pack(pack_seq, cmd_list['get_secret'], 4, padding)
        msg = cmd_list['get_secret'].to_bytes(4,'big')
        msg += b'\x04'
        msg += token.to_bytes(4,'big')
        s.bind(('127.0.0.1', accept_port_2))
        host = ('127.0.0.1', 1337)
        s.connect(host)
        s.sendall(msg)       
        resp = s.recv(1024)
        code = int(resp[:4].hex(),16)
        final = resp[5:]
        return (code, final)
        
