#include <stdio.h>
#include <stdlib.h>
#include "TestCode.h"

// Refer to README.md for the problem instructions

int cmpfnc(int *a, int *b)
{
    return (*a - *b);
}

int arrayManipulate(int *arr, unsigned int size)
{
    // test rule size
    if (size < 2)
    {
        return -1;
    }

    // check null
    if(!arr)
        return -1;
        
    // apply rules 1 or 2
    for (int i = 0; i < size; i++)
    {
        // rule 1 update
        
        if(*(arr+i) % 2 == 0 && *(arr+i) > 6)
        {
            // square it
            *(arr+i) *= *(arr+i);
            continue;
        }
        if(*(arr+i) % 2 != 0 || *(arr+i) > 2)
        {
            // double it
            *(arr+i) *= 2;
            continue;
        }
    }

    // sort the array
    qsort(arr, size, sizeof(int), cmpfnc);

    // determine what to return
    if(size % 2 == 0)
    {
        return arr[size/2] + arr[size/2 - 1];
    }
    else
    {
        return arr[size/2];
    }
    
    return 0;
}
