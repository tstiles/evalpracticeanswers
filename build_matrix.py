# Refer to README.md for the problem instructions


def buildMatrix(rows, cols):
    if rows <= 1 or cols <= 1 :
        return []

    matrix = [[x*y for y in range(cols)] for x in range(rows)]

    if rows % 2 != 0:
        if rows == cols:
            center = int(rows/2)
            matrix[center][center] = 1

    return matrix
